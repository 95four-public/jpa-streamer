package com.demo.jpastreamer.service;

import com.demo.jpastreamer.persistance.UserDomain;
import com.demo.jpastreamer.persistance.UserDomain$;
import com.demo.jpastreamer.persistance.UserRepository;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final JPAStreamer jpaStreamer;

    public UserDomain createUser(UserDomain userDomain) {
        return userRepository.save(userDomain);
    }

    /**
     * get first 3 user domains with first name starts with A
     *
     * @return
     */
    public List<UserDomain> getUsers() {
        return jpaStreamer.stream(UserDomain.class)
                .filter(UserDomain$.firstName.startsWith("A"))
                .sorted(UserDomain$.firstName)
                .limit(3)
                .collect(Collectors.toList());
    }
}
