package com.demo.jpastreamer.controller;

import com.demo.jpastreamer.persistance.UserDomain;
import com.demo.jpastreamer.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/demo")
@RequiredArgsConstructor
public class DemoController {

    private final UserService userService;

    @PostMapping
    public UserDomain createUser(@RequestBody UserDomain userDomain) {
        return userService.createUser(userDomain);
    }

    @GetMapping
    public List<UserDomain> getUsers() {
        return userService.getUsers();
    }
}
